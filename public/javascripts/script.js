$.when( $.ready ).then(function() {
    console.log('jquery is ready');
    let data=[];  
    let counter=1;
    let interval;
    
    $.ajax({
        url: "https://collectionapi.metmuseum.org/public/collection/v1/objects?metadataDate=2021-09-22",
        context: document.body,
        headers:{'User-Agent':'Mustafa','Accept':'*/*','Accept-Encoding':'gzip, deflate, br','Content-Type':'application/json','X-Requested-With':'XMLHttpRequest'}
    }).done(function(result) {
        data=result.objectIDs;
        $.ajax({
            url: "https://collectionapi.metmuseum.org/public/collection/v1/objects/"+data[0],
            context: document.body
        }).done(function(result1) {
            if(result1.primaryImage!==''){
                $("#image").attr('src',result1.primaryImage);
            } else {
                $("#image").attr('src',"images/not_available.png");
            }
            $("#title").html("<b>Title</b>: "+result1.title);
            $("#accessionNumber").html("<b>Accession Number</b>: "+result1.accessionNumber);
            $("#accessionYear").html("<b>Accession Year</b>: "+result1.accessionYear);
            $("#department").html("<b>Department</b>: "+result1.department);
            $("#objectName").html("<b>Object Name</b>: "+result1.objectName);
            $("#artistName").html("<b>Artist Name</b>: "+result1.artistDisplayName);
            $("#artistBio").html("<b>Artist Bio</b>: "+result1.artistDisplayBio);
        });

        interval=getObjectDetails()
    

    });


    $("#stop-slide-show").on("click",()=>{
        clearInterval(interval);
        $("#detailInfo").removeClass("hidden");
        $("#stop-slide-show").addClass("hidden");
        $("#resume-slide-show").removeClass("hidden");
    });
 

    $("#resume-slide-show").on("click",()=>{
        $("#detailInfo").addClass("hidden");
        getObjectDetails();
        $("#stop-slide-show").removeClass("hidden");
        $("#resume-slide-show").addClass('hidden');
    });

    function getObjectDetails(){
        
        interval= setInterval(()=>{
            $.ajax({
                url: "https://collectionapi.metmuseum.org/public/collection/v1/objects/"+data[counter],
                context: document.body
            }).done(function(result1) {
                if(result1.primaryImage!==''){
                    $("#image").attr('src',result1.primaryImage);
                } else {
                    $("#image").attr('src',"images/not_available.png");
                }
                $("#title").html("<b>Title</b>: "+result1.title);
                $("#accessionNumber").html("<b>Accession Number</b>: "+result1.accessionNumber);
                $("#accessionYear").html("<b>Accession Year</b>: "+result1.accessionYear);
                $("#department").html("<b>Department</b>: "+result1.department);
                $("#objectName").html("<b>Object Name</b>: "+result1.objectName);
                $("#artistName").html("<b>Artist Name</b>: "+result1.artistDisplayName);
                $("#artistBio").html("<b>Artist Bio</b>: "+result1.artistDisplayBio);
            });

            counter++;

        },10000);

        return interval;
    }
    
  });